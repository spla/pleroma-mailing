#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
from datetime import datetime, timezone, timedelta
import time
import threading
import os
import sys
import os.path
import psycopg2

##############################################################################
# ask what user to be edit
##############################################################################

def ask_what_user():

  try:

    useremail = input("Enter user email address: (press q to quit) ")
    if useremail == 'q':
      sys.exit()
    else:
      user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback = get_user(useremail)
      return (user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback)
  except:

    if useremail != 'q':
      print("Enter an existent email address!")
    sys.exit("Good bye!")

###############################################################################
# get user row
###############################################################################

def get_user(email):

  try:

    conn = psycopg2.connect(database = mailing_db, user = mailing_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("SELECT account_id, username, email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, feedback  FROM " + mailing_db_table + " where email=(%s)", (email,))

    row = cur.fetchone()

    user_id = row[0]
    user_name = row[1]
    user_email = row[2]
    emailed_at = row[3].replace(tzinfo=None)
    deleted = row[4]
    elapsed_days = row[5]
    to_be_deleted = row[6]
    recipient_error = row[7]
    user_feedback = row[8]

    cur.close()
    return (user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback)

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

###################################################################################
# write to database mailing status of inactive users
###################################################################################

def update_user(will_be_deleted, recip_error, user_feedback, id):

  conn = None

  try:

    conn = psycopg2.connect(database = mailing_db, user = mailing_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    cur.execute("UPDATE " + mailing_db_table + " SET to_be_deleted=(%s), recipient_error=(%s), feedback=(%s) where account_id=(%s)", (will_be_deleted, recip_error, user_feedback, id))
    print("\n")
    print("Updating user " + str(id))

    conn.commit()

    cur.close()

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

##################################################################################
# print user data
##################################################################################

def print_user(user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback):

  print("\n")
  print("--------------------------------------------------------------------------------------------------------------------------------")
  print("| account_id: " + str(user_id) + "  | username: " + str(user_name) + "                      | email: " + str(user_email) + " | emailed at: " + str(emailed_at) + " |")
  print("| deleted: " + str(deleted) + " | elapsed days: " + str(elapsed_days) + " | to be deleted: " + str(to_be_deleted) + " | recipient error: " + str(recipient_error) + " | user feedback: " +str(user_feedback) + " |")
  print("--------------------------------------------------------------------------------------------------------------------------------")
  print("\n")

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        if file_path == "secrets/secrets.txt":
          print("File %s not found, exiting. Run setup.py."%file_path)
        elif file_path == "config.txt":
          print("File %s not found, exiting. Run db-setup.py."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    print("Run setup.py")
    sys.exit(0)

# Load configuration from config file
config_filepath = "config.txt"
pleroma_db = get_parameter("pleroma_db", config_filepath)
pleroma_db_user = get_parameter("pleroma_db_user", config_filepath)
mailing_db = get_parameter("mailing_db", config_filepath)
mailing_db_user = get_parameter("mailing_db_user", config_filepath)
mailing_db_table = get_parameter("mailing_db_table", config_filepath)

###############################################################################

while True:

  user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback = ask_what_user()

  print_user(user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback)

  willdeleteit = input("Do you want to mark user to be deleted? (True or False) ")
  if willdeleteit == 'f':
     willdeleteit = 'False'
  elif willdeleteit == 't':
     willdeleteit = 'True'

  recip_error = input("Was the email refused? (True or False) ")
  if recip_error == 'f':
     recip_error = 'False'
  elif recip_error == 't':
     recip_error = 'True'

  user_feed = input("Have the user replied? (True or False) ")
  if user_feed == 'f':
     user_feed = 'False'
  elif user_feed == 't':
     user_feed = 'True'

  update_user(willdeleteit, recip_error, user_feed, user_id)
  user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback = get_user(user_email)
  print_user(user_id, user_name, user_email, emailed_at, deleted, elapsed_days, to_be_deleted, recipient_error, user_feedback)
