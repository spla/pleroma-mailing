# pleroma-mailing

Mail the inactive users of your Pleroma server

This code written in Python get all your inactive users from Pleroma's database and email them with the subject and message of your choice.
Then, inactive users data is stored into new created Postgresql database to track feedback and status.
Run mailing.py periodically to catch 'new' inactive users and update the elapsed days of the already emailed ones.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Pleroma server (admin)
-   Everything else at the top of `pleroma_mailing.py`!

### Usage:

Within your Python Virtual Environment:

1. Run 'db-setup.py' to set database parameters (saved to config.txt file) and create needed database and table. All collected data of inactive users (see point 3) will be written there. 

2. Run 'setup.py' to set your SMTP parameters and desired email subject. Also set here 'inactive days' parameter. They will be saved to 'secrets/secrets.txt' for further use.

3. Run 'pleroma_mailing.py' to start emailing your inactive users (older than 'inactive days' parameter). Their username, account_id, email, delivery status (True if successful) and delivery date will be written to Postgresql database. There is another column, 'deleted', False by default. Will be useful to track deleted/not deleted inactive users if you choose to do so.

4. Use your favourite scheduling method to set mailing.py to run regularly. Column 'elapsed_days' of mailing's database will be updated so you can decide actions after some time.

5. Run 'editstatus.py' to change/edit any inactive user's flags.

Note: install all needed packages with 'pip install -r requirements.txt'
