#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
import getpass
import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  print("Setting up mailing DB parameters...")
  print("\n")
  pleroma_db = input("Pleroma db name: ")
  pleroma_db_user = input("Pleroma db user: ")
  mailing_db = input("Mailing db name: ")
  mailing_db_user = input("Mailing db user: ")
  mailing_db_table = input("Mailing db table: ")
  with open(file_path, "w") as text_file:
    print("pleroma_db: {}".format(pleroma_db), file=text_file)
    print("pleroma_db_user: {}".format(pleroma_db_user), file=text_file)
    print("mailing_db: {}".format(mailing_db), file=text_file)
    print("mailing_db_user: {}".format(mailing_db_user), file=text_file)
    print("mailing_db_table: {}".format(mailing_db_table), file=text_file)

def create_table(db, db_user, table, sql):

  try:

    conn = None
    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()


    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################
  
# Load configuration from config file
config_filepath = "config.txt"
pleroma_db = get_parameter("pleroma_db", config_filepath) # E.g., mastodon_production
pleroma_db_user = get_parameter("pleroma_db_user", config_filepath) # E.g., mastodon
mailing_db = get_parameter("mailing_db", config_filepath) # E.g., inactive
mailing_db_user = get_parameter("mailing_db_user", config_filepath) # E.g., mastodon
mailing_db_table = get_parameter("mailing_db_table", config_filepath) # E.g., inactive_users 

############################################################
# create database
############################################################

try:

  conn = psycopg2.connect(dbname='postgres',
      user=mailing_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + mailing_db + ". Please wait...")
  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(mailing_db))
      )
  print("Database " + mailing_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = mailing_db, user = mailing_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("config.txt")
  print("Exiting. Run setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Mailing db parameters saved to config.txt!")
  print("\n")

############################################################
# Create needed tables 
############################################################

print("Creating table...")

########################################

db = mailing_db
db_user = mailing_db_user
table = mailing_db_table
sql = '''create table {table}(datetime timestamptz, account_id uuid primary key, username varchar(30), email varchar(50), emailed_at timestamptz, emailed boolean default False, deleted boolean default False, 
         elapsed_days varchar(30), to_be_deleted boolean default False, recipient_error boolean default False, feedback boolean default False)'''.format(table=mailing_db_table)
create_table(db, db_user, table, sql)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
